/*
 * Copyright 2019-2021 Keith Russo Licensed under the
 * 	Educational Community License, Version 2.0 (the "License"); you may
 * 	not use this file except in compliance with the License. You may
 * 	obtain a copy of the License at
 *
 * 	http://www.osedu.org/licenses/ECL-2.0
 *
 * 	Unless required by applicable law or agreed to in writing,
 * 	software distributed under the License is distributed on an "AS IS"
 * 	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * 	or implied. See the License for the specific language governing
 * 	permissions and limitations under the License.
 */

package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"
	"runtime"
	"syscall"

	"gitlab.com/aguemort/ATSIR/commandhandler"
	"gitlab.com/aguemort/ATSIR/plugins"
	"gitlab.com/aguemort/ATSIR/plugins/starboard"
	"gitlab.com/aguemort/ATSIR/utilities"

	"github.com/bwmarrin/discordgo"
	"github.com/mattn/go-colorable"
	log "github.com/sirupsen/logrus"
)

func init() {
	// Console logging fix for Windows systems
	if runtime.GOOS == "windows" {
		log.SetFormatter(&log.TextFormatter{ForceColors: true})
		log.SetOutput(colorable.NewColorableStdout())
	}
	log.SetLevel(log.TraceLevel)

	utilities.Info("Initializing bot settings...")
	utilities.Settings = *utilities.BotSettings() // ONLY call once unless programmatically changing settings
	utilities.Info("Bot settings initialized...")

	// Weather plugin not implemented
	//log.Infoln("Initializing weather plugin...")
	//utilities.WeatherAPICityData()
	//log.Infoln("Weather plugin initialized...")

	utilities.Info("Starting bot...")
}

func main() {
	bot, err := discordgo.New("Bot " + utilities.Settings.DiscordToken)
	if err != nil {
		utilities.Fatal("Error creating Discord session", err)
		os.Exit(1)
	}

	utilities.Info("Initializing commands")
	ch := commandhandler.New(utilities.Settings.Prefix, utilities.Settings.IgnoreBots)

	// Register all commands here until I implement auto command discovery and enable/disable hooks
	// ch.AddCommand("admin", &plugins.Admin) // Not implemented
	ch.AddCommand("art", &plugins.ASCII)
	ch.AddCommand("cat", &plugins.Cats)
	ch.AddCommand("dog", &plugins.Dogs)
	ch.AddCommand("help", &plugins.Help)
	ch.AddCommand("invite", &plugins.Invite)
	ch.AddCommand("movie", &plugins.Movie)
	ch.AddCommand("starboard", &plugins.Starboard)
	ch.AddCommand("twitch", &plugins.Twitch)

	utilities.Info(len(ch.Commands), "command(s) initialized")

	// Register event listeners here
	utilities.Debug("Adding event handlers...")
	bot.AddHandler(ch.OnMessage)
	bot.AddHandler(starboard.OnMessageReactionAdd)
	bot.AddHandler(starboard.OnMessageReactionRemove)
	bot.AddHandler(starboard.OnMessageReactionRemoveAll)

	err = bot.Open()
	if err != nil {
		utilities.Fatal("Error opening WebSocket connection", err)
	}

	// We need to add the bots name programmatically to the settings file; we only need to do it once
	if utilities.Settings.BotName == "" {
		utilities.Settings.BotName = bot.State.User.Username
		file, _ := json.MarshalIndent(&utilities.Settings, "", "  ")
		_ = ioutil.WriteFile("data/bot.settings", file, 0644)
	}

	botUser, _ := bot.User(utilities.Settings.DiscordClientID)
	utilities.Settings.BotName = botUser.Username

	// ASCII Art in logger
	fmt.Println(utilities.ASCIIArt(utilities.Settings.BotName))

	utilities.Info("The bot is now running.  Press CTRL-C to exit.")
	utilities.Info("Bot's Invite Link:  https://discordapp.com/oauth2/authorize?&client_id=" +
		utilities.Settings.DiscordClientID +
		"&scope=bot&permissions=8")

	// Goroutines here
	utilities.Debug("Starting GoRoutines...")

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	if bot.Close() != nil {
		utilities.Fatal(err)
	}
}
