/*
 * Copyright 2019-2021 Keith Russo Licensed under the
 * 	Educational Community License, Version 2.0 (the "License"); you may
 * 	not use this file except in compliance with the License. You may
 * 	obtain a copy of the License at
 *
 * 	http://www.osedu.org/licenses/ECL-2.0
 *
 * 	Unless required by applicable law or agreed to in writing,
 * 	software distributed under the License is distributed on an "AS IS"
 * 	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * 	or implied. See the License for the specific language governing
 * 	permissions and limitations under the License.
 */

package commandhandler

import (
	"bytes"
	"flag"
	"sort"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
)

const (
	// CommandsList - List all commands available to any user
	CommandsList = iota
	// HelpTopicsList - All help specific commands
	HelpTopicsList
	// Moderator - Mod only commands
	Moderator
	// Unlisted - Admin/owner only commands
	Unlisted
)

// ListSection - Used to put certain commands in different categories in the command list
type ListSection int8

// Command - holds all pertinent data for executing and parsing the command
// DO NOT CHANGE THIS UNLESS YOU KNOW WHAT YOU'RE DOING!
type Command struct {
	// UsageLine - Short usage message; first word is the command name
	UsageLine string

	// ShortDescription - The short description of the command; < 256 chars
	ShortDescription string

	// LongDescription - The long description of the command; < 1024 chars
	LongDescription string

	// List - Which section of the command list to display this command
	List ListSection

	// Run - Runs this command; args are everything after the command name
	Run func(cmd *Command, args []string) error

	// Flag - List of UNIX style flags to use with this command
	Flag flag.FlagSet

	// SubCommands - List of available sub-commands
	SubCommands []*Command

	// Parent - The parent command; use nil for root command
	Parent *Command

	// UsageTemplate - The short description on how to use this command; empty for default
	UsageTemplate string

	// HelpFields - The Discord Embed fieldset used for displaying help
	HelpFields []*discordgo.MessageEmbedField

	// RequiredPermissions - The Discord permissions required to use this command; empty for everyone
	RequiredPermissions []string

	// Message - The MessageCreate event from discordgo
	Message *discordgo.MessageCreate

	// Session - The Session from discordgo
	Session *discordgo.Session
}

// Name returns the name of the command; the first word of the UsageLine
func (c *Command) Name() string {
	var name = ""
	index := strings.Index(c.UsageLine, " ")
	if index > 0 {
		name = c.UsageLine[:index]
	}

	return name
}

// Usage returns the usage details
func (c *Command) Usage() {
	c.usage()
}

// FlagOptions returns the flag's options as a string
func (c *Command) FlagOptions() string {
	var buf bytes.Buffer
	c.Flag.SetOutput(&buf)
	c.Flag.PrintDefaults()

	str := string(buf.Bytes())
	if len(str) > 0 {
		return str
	}
	// If no flags are set for the command
	return ""
}

// Runnable reports whether the command is runnable by the user that invoked it
func (c *Command) Runnable() bool {
	if c.RequiredPermissions != nil {
		// TODO: Perform advanced permissions checks
	}
	return true
}

// CommandSlice - Allows for using sort.Sort on a slice of commands
type CommandSlice []*Command

// Len - Utility functions on a slice of Commands
func (c CommandSlice) Len() int {
	return len(c)
}

// Less - Compares two command names
func (c CommandSlice) Less(a, b int) bool {
	return c[a].Name() < c[b].Name()
}

// Swap - swap the position of two commands in the command list
func (c CommandSlice) Swap(a, b int) {
	c[a], c[b] = c[b], c[a]
}

// SortCommands - Sort commands (internally) alphabetically
func (c *Command) SortCommands() {
	sort.Sort(CommandSlice(c.SubCommands))
}

// init - Initialize the command
func (c *Command) init() {
	// If there is no parent command, then it's already initialized
	if c.Parent != nil {
		return
	}

	for _, cmd := range c.SubCommands {
		// Initialize command hierarchy
		cmd.Parent = c
		// Initialize sub-commands
		cmd.init()
	}
}

// Dispatch executes the command using the provided arguments
// If a sub-command exists matching the first argument, it is dispatched
// Otherwise, the command's Run function is called
func (c *Command) Dispatch(args []string) error {
	if c == nil {
		logrus.Warningln("Called Run() on a nil command")
		return nil
	}

	// Ensure the command is initialized
	c.init()

	// Try looking for a valid sub-command
	if len(args) > 0 {
		for _, cmd := range c.SubCommands {
			name := cmd.Name()
			// If the sub-command exists, dispatch to it
			if name == strings.ToLower(args[0]) {
				// Make sure the sub-command is initialized
				return cmd.Dispatch(args[1:])
			}
		}

		// If help context is invoked
		if strings.ToLower(args[0]) == "help" {
			return c.help(args[1:])
		}
	}

	// If no sub-command is present, then run this command
	if c.Runnable() {
		// TODO: add flag parsing of UNIX style - and -- flags
		return c.Run(c, args)
	}

	// TODO: add command alias parsing

	return nil
}

func (c *Command) usage() string {
	c.SortCommands()

	return c.UsageLine
}

// help outputs the help information
func (c *Command) help(args []string) error {
	// Hack to make sure the Message and Session are initialized in the sub command
	if (c.Message == nil || c.Session == nil) && c.Parent != nil {
		c.Message = c.Parent.Message
		c.Session = c.Parent.Session
	}

	title := "Command: " + c.usage()
	description := c.ShortDescription

	embed := &discordgo.MessageEmbed{
		Type:        "rich",
		Title:       title,
		Description: description,
		Timestamp:   time.Now().Format(time.RFC3339),
		Color:       6570404,
		Footer: &discordgo.MessageEmbedFooter{
			Text: "Help requested by: " + c.Message.Author.Username,
		},
		Author: &discordgo.MessageEmbedAuthor{
			URL:  "",
			Name: c.FullSpacedName() + " help",
		},
		Fields: c.HelpFields,
	}

	msg, err := c.Session.ChannelMessageSendEmbed(c.Message.ChannelID, embed)
	if err != nil {
		logrus.Warningln("Error sending the embed to Discord", err)
		return nil
	}
	logrus.Infoln("Message", msg.ID, "sent to Guild:", msg.GuildID)

	_ = c.Session.ChannelMessageDelete(c.Message.ChannelID, c.Message.ID)

	return nil
}

// FullSpacedName - Retrieve the fully qualified name of this command
func (c *Command) FullSpacedName() string {
	name := c.Name()
	// If no other parent command exists
	if c.Parent != nil {
		// recurse until no parent is found
		name = c.Parent.FullSpacedName() + " " + name
	}

	return name
}

// SubCommandList - Returns the sorted sub-command list
func (c *Command) SubCommandList(list ListSection) []*Command {
	var commands []*Command

	for _, cmd := range c.SubCommands {
		if cmd.List == list {
			commands = append(commands, cmd)
		}
	}

	return commands
}
