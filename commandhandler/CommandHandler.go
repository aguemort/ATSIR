/*
 * Copyright 2019-2021 Keith Russo Licensed under the
 * 	Educational Community License, Version 2.0 (the "License"); you may
 * 	not use this file except in compliance with the License. You may
 * 	obtain a copy of the License at
 *
 * 	http://www.osedu.org/licenses/ECL-2.0
 *
 * 	Unless required by applicable law or agreed to in writing,
 * 	software distributed under the License is distributed on an "AS IS"
 * 	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * 	or implied. See the License for the specific language governing
 * 	permissions and limitations under the License.
 */

package commandhandler

import (
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/aguemort/ATSIR/structs"
	"gitlab.com/aguemort/ATSIR/utilities"
)

// CommandHandler - the main object that holds all active commands and their associated data
type CommandHandler struct {
	Prefix     string
	Commands   map[string]*structs.Command
	IgnoreBots bool
	Session    *discordgo.Session
	Message    *discordgo.MessageCreate
}

// New - Creates a new command handler
func New(prefix string, ignoreBots bool) *CommandHandler {
	return &CommandHandler{
		Prefix:     utilities.Settings.Prefix,
		Commands:   map[string]*structs.Command{},
		IgnoreBots: utilities.Settings.IgnoreBots,
	}
}

// AddCommand - Add a command on the fly
func (ch *CommandHandler) AddCommand(name string, command *structs.Command) {
	ch.Commands[name] = command
}

// RemoveCommand - Remove a command on the fly
func (ch *CommandHandler) RemoveCommand(name string) {
	delete(ch.Commands, name)
}

// OnMessage - The event handler for incoming messages
func (ch *CommandHandler) OnMessage(session *discordgo.Session, msg *discordgo.MessageCreate) {
	// Ignore all messages from other bots
	if ch.IgnoreBots && msg.Author.Bot {
		return
	}

	// Split the message into a slice for command parsing
	msgSplit := strings.Split(msg.Content, " ")

	// Check for a command initializer and if found, strip it out
	if msgSplit[0] == "<@"+utilities.Settings.DiscordClientID+">" ||
		msgSplit[0] == "<@!"+utilities.Settings.DiscordClientID+">" ||
		msgSplit[0] == ch.Prefix {
		msgSplit = msgSplit[1:]
	} else if len(msgSplit[0]) >= len(ch.Prefix) && msgSplit[0][0:len(ch.Prefix)] == ch.Prefix {
		msgSplit[0] = msgSplit[0][len(ch.Prefix):]
	} else {
		// If a command initializer is not found, we return as it's not a called command
		return
	}

	utilities.Debug("Command for the bot detected!")

	utilities.Trace(msgSplit)

	// Check to see if the command exists in the command handler
	if command, ok := ch.Commands[strings.ToLower(msgSplit[0])]; ok {
		utilities.Debug("Command found! Preparing to dispatch the command")
		command.Message = msg
		command.Session = session
		err := command.Dispatch(msgSplit[1:])
		if err != nil {
			utilities.Error(err)
		}
	}
}
