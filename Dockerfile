FROM golang:latest
MAINTAINER Keith Russo <aguemort@outlook.com>

# Install golint
ENV GOPATH /go
ENV PATH ${GOPATH}/bin:$PATH
RUN go get -u golang.org/x/lint/golint