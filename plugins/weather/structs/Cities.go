/*
 * Copyright 2019-2021 Keith Russo Licensed under the
 * 	Educational Community License, Version 2.0 (the "License"); you may
 * 	not use this file except in compliance with the License. You may
 * 	obtain a copy of the License at
 *
 * 	http://www.osedu.org/licenses/ECL-2.0
 *
 * 	Unless required by applicable law or agreed to in writing,
 * 	software distributed under the License is distributed on an "AS IS"
 * 	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * 	or implied. See the License for the specific language governing
 * 	permissions and limitations under the License.
 */

package weather

// Cities - Structure for city data; will be deprecated soon
type Cities struct {
	City []struct {
		ID          int    `json:"id"`
		Name        string `json:"name"`
		Country     string `json:"country"`
		Coordinates struct {
			Longitude float32 `json:"lon"`
			Latitude  float32 `json:"lat"`
		} `json:"coord"`
	}
}
