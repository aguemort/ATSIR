/*
 * Copyright 2019-2021 Keith Russo Licensed under the
 * 	Educational Community License, Version 2.0 (the "License"); you may
 * 	not use this file except in compliance with the License. You may
 * 	obtain a copy of the License at
 *
 * 	http://www.osedu.org/licenses/ECL-2.0
 *
 * 	Unless required by applicable law or agreed to in writing,
 * 	software distributed under the License is distributed on an "AS IS"
 * 	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * 	or implied. See the License for the specific language governing
 * 	permissions and limitations under the License.
 */

package admin

import (
	"encoding/json"
	"errors"
	"io/ioutil"

	"gitlab.com/aguemort/ATSIR/structs"
	"gitlab.com/aguemort/ATSIR/utilities"
)

// LogLevel - Primary handler for setting the logging level
func LogLevel(ch *structs.Command, args []string) error {
	if len(args) != 1 {
		return errors.New("missing logging level")
	}

	switch args[0] {
	case "0":
	case "trace":
		utilities.Settings.LogLevel = structs.Trace
		break
	case "1":
	case "debug":
		utilities.Settings.LogLevel = structs.Debug
		break
	case "2":
	case "info":
		utilities.Settings.LogLevel = structs.Info
		break
	case "3":
	case "warn":
	case "warning":
		utilities.Settings.LogLevel = structs.Warning
		break
	case "4":
	case "error":
		utilities.Settings.LogLevel = structs.Error
		break
	case "5":
	case "fatal":
		utilities.Settings.LogLevel = structs.Fatal
		break
	}

	file, _ := json.MarshalIndent(utilities.Settings, "", "  ")
	_ = ioutil.WriteFile("data/bot.settings", file, 0644)

	return nil
}
