/*
 * Copyright 2019-2021 Keith Russo Licensed under the
 * 	Educational Community License, Version 2.0 (the "License"); you may
 * 	not use this file except in compliance with the License. You may
 * 	obtain a copy of the License at
 *
 * 	http://www.osedu.org/licenses/ECL-2.0
 *
 * 	Unless required by applicable law or agreed to in writing,
 * 	software distributed under the License is distributed on an "AS IS"
 * 	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * 	or implied. See the License for the specific language governing
 * 	permissions and limitations under the License.
 */

package cat

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/aguemort/ATSIR/structs"
	"gitlab.com/aguemort/ATSIR/utilities"
)

type catSettingsStruct struct {
	APIKey string
}

type catData []struct {
	ID               string  `json:"id"`
	URL              string  `json:"url"`
	Height           *int16  `json:"height,omitempty"`
	Width            *int16  `json:"width,omitempty"`
	BreedIds         *string `json:"breed_ids,nullable,omitempty"`
	SubID            *string `json:"sub_id,omitempty"`
	CreatedAt        *string `json:"created_at,omitempty"`
	OriginalFilename *string `json:"original_filename,omitempty"`
	Categories       *[]struct {
		ID   *int16  `json:"id,omitempty"`
		Name *string `json:"name,omitempty"`
	} `json:"categories,omitempty"`
	Breeds []struct {
		ID               *string `json:"id,omitempty"`
		Name             *string `json:"name,omitempty"`
		Adaptability     *int    `json:"adaptability,omitempty"`
		AffectionLevel   *int    `json:"affection_level,omitempty"`
		AltNames         *string `json:"alt_names,omitempty"`
		CfaURL           *string `json:"cfa_url,omitempty"`
		ChildFriendly    *int    `json:"child_friendly,omitempty"`
		CountryCode      *string `json:"country_code,omitempty"`
		CountryCodes     *string `json:"country_codes,omitempty"`
		Description      *string `json:"description,omitempty"`
		DogFriendly      *int    `json:"dog_friendly,omitempty"`
		EnergyLevel      *int    `json:"energy_level,omitempty"`
		Experimental     *int    `json:"experimental,omitempty"`
		Grooming         *int    `json:"grooming,omitempty"`
		Hairless         *int    `json:"hairless,omitempty"`
		HealthIssues     *int    `json:"health_issues,omitempty"`
		Hypoallergenic   *int    `json:"hypoallergenic,omitempty"`
		Indoor           *int    `json:"indoor,omitempty"`
		Intelligence     *int    `json:"intelligence,omitempty"`
		Lap              *int    `json:"lap,omitempty"`
		LifeSpan         *string `json:"life_span,omitempty"`
		Natural          *int    `json:"natural,omitempty"`
		Origin           *string `json:"origin,omitempty"`
		Rare             *int    `json:"rare,omitempty"`
		Rex              *int    `json:"rex,omitempty"`
		SheddingLevel    *int    `json:"shedding_level,omitempty"`
		ShortLegs        *int    `json:"short_legs,omitempty"`
		SocialNeeds      *int    `json:"social_needs,omitempty"`
		StrangerFriendly *int    `json:"stranger_friendly,omitempty"`
		SuppressTail     *int    `json:"suppress_tail,omitempty"`
		Temperament      *string `json:"temperament,omitempty"`
		VcahospitalsURL  *string `json:"vcahospitals_url,omitempty"`
		VetstreetURL     *string `json:"vetstreet_url,omitempty"`
		Vocalisation     *int    `json:"vocalisation,omitempty"`
		Weight           *struct {
			Imperial *string `json:"imperial,omitempty"`
			Metric   *string `json:"metric,omitempty"`
		} `json:"weight,omitempty"`
		WeightImperial *string `json:"weight_imperial,omitempty"`
		WikipediaURL   *string `json:"wikipedia_url,omitempty"`
	} `json:"breeds,omitempty"`
}

var catSettings = &catSettingsStruct{}

func init() {
	if _, err := os.Stat("data/cat.settings"); os.IsNotExist(err) {
		utilities.Info("Cats settings file not found.  Creating it...")
		catSettings.APIKey = "80478b42-8dc2-4fb3-a429-238ea17c29d4"

		file, _ := json.MarshalIndent(catSettings, "", "  ")
		_ = ioutil.WriteFile("data/cat.settings", file, 0644)
	}

	settingsJSON, _ := utilities.ReadJSONFile("data/cat.settings")
	_ = json.Unmarshal(settingsJSON, catSettings)
}

// GetRandomCat - The primary handler for the Cat command
func GetRandomCat(ch *structs.Command, args []string) error {
	client := http.DefaultClient
	request, err := http.NewRequest("GET", "https://api.thecatapi.com/v1/images/search?has_breeds=true", nil)
	if request == nil || err != nil {
		utilities.Warning("Error setting up the http request", err)
		return nil
	}
	request.Header.Set("x-api-key", catSettings.APIKey)

	response, err := client.Do(request)
	if err != nil {
		utilities.Warning("Error processing the client request", err)
		return nil
	}
	defer response.Body.Close()

	if response == nil {
		utilities.Warning("We received a nil response", response)
		return nil
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		utilities.Warning("Error reading the response body", err)
	}
	imageData := catData{}
	err = json.Unmarshal(body, &imageData)
	if err != nil {
		utilities.Warning("Error unmarshalling the response JSON", err)
		return nil
	}

	if len(imageData) > 0 {
		buildCatEmbed(ch, args, imageData)
	}

	return nil
}

func buildCatEmbed(ch *structs.Command, args []string, cat catData) {
	var fields []*discordgo.MessageEmbedField
	c := cat[0]

	// The base embed
	embed := &discordgo.MessageEmbed{
		URL:         cat[0].URL,
		Type:        "rich",
		Title:       "",
		Description: "",
		Timestamp:   time.Now().Format(time.RFC3339),
		Color:       0,
		Footer: &discordgo.MessageEmbedFooter{
			Text:    "Cat image and data provided by: https://thecatapi.com/",
			IconURL: "https://cdn2.thecatapi.com/logos/thecatapi_256xW.png",
		},
		Image: &discordgo.MessageEmbedImage{
			URL: c.URL,
		},
		Thumbnail: &discordgo.MessageEmbedThumbnail{
			URL: "https://cdn2.thecatapi.com/logos/thecatapi_256xW.png",
		},
	}

	// Build the embed fields if additional metadata is provided from the API
	if len(c.Breeds) > 0 {
		b := c.Breeds[0]

		if b.Adaptability != nil {
			fields = append(fields, &discordgo.MessageEmbedField{
				Name:   "Adaptability",
				Value:  rangeEmoji(*b.Adaptability),
				Inline: true,
			})
		}
		if b.AffectionLevel != nil {
			fields = append(fields, &discordgo.MessageEmbedField{
				Name:   "Affection",
				Value:  rangeEmoji(*b.AffectionLevel),
				Inline: true,
			})
		}
		if b.ChildFriendly != nil {
			fields = append(fields, &discordgo.MessageEmbedField{
				Name:   "Child Friendly",
				Value:  rangeEmoji(*b.ChildFriendly),
				Inline: true,
			})
		}
		if b.Description != nil {
			embed.Description = *b.Description
		}
		if b.DogFriendly != nil {
			fields = append(fields, &discordgo.MessageEmbedField{
				Name:   "Doggo Friendly",
				Value:  rangeEmoji(*b.DogFriendly),
				Inline: true,
			})
		}
		if b.EnergyLevel != nil {
			fields = append(fields, &discordgo.MessageEmbedField{
				Name:   "Energy Level",
				Value:  rangeEmoji(*b.EnergyLevel),
				Inline: true,
			})
		}
		if b.Grooming != nil {
			fields = append(fields, &discordgo.MessageEmbedField{
				Name:   "Grooming",
				Value:  rangeEmoji(*b.Grooming),
				Inline: true,
			})
		}
		if b.Hairless != nil {
			fields = append(fields, &discordgo.MessageEmbedField{
				Name:   "Hairless",
				Value:  boolEmoji(*b.Hairless),
				Inline: true,
			})
		}
		if b.HealthIssues != nil {
			fields = append(fields, &discordgo.MessageEmbedField{
				Name:   "Health Issues",
				Value:  rangeEmoji(*b.HealthIssues),
				Inline: true,
			})
		}
		if b.Intelligence != nil {
			fields = append(fields, &discordgo.MessageEmbedField{
				Name:   "Intelligence",
				Value:  rangeEmoji(*b.Intelligence),
				Inline: true,
			})
		}
		if b.Name != nil {
			embed.Author = &discordgo.MessageEmbedAuthor{
				Name: *b.Name,
			}
			if b.VetstreetURL != nil {
				embed.Author.URL = *b.VetstreetURL
			}
		}
		if b.SheddingLevel != nil {
			fields = append(fields, &discordgo.MessageEmbedField{
				Name:   "Shedding Level",
				Value:  rangeEmoji(*b.SheddingLevel),
				Inline: true,
			})
		}
	}

	// If no meta is returned for the image, we don't send the fields (for right now)
	if fields != nil {
		embed.Fields = fields
	}

	go utilities.SendEmbedMessage(ch, "", embed)
}

func boolEmoji(val int) string {
	if val == 1 {
		return " :white_check_mark: "
	}
	return " :x: "
}

func rangeEmoji(val int) string {
	if val == 1 {
		return " :one: "
	} else if val == 2 {
		return " :two: "
	} else if val == 3 {
		return " :three: "
	} else if val == 4 {
		return " :four: "
	}

	return " :five: "
}
