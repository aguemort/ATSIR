/*
 * Copyright 2019-2021 Keith Russo Licensed under the
 * 	Educational Community License, Version 2.0 (the "License"); you may
 * 	not use this file except in compliance with the License. You may
 * 	obtain a copy of the License at
 *
 * 	http://www.osedu.org/licenses/ECL-2.0
 *
 * 	Unless required by applicable law or agreed to in writing,
 * 	software distributed under the License is distributed on an "AS IS"
 * 	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * 	or implied. See the License for the specific language governing
 * 	permissions and limitations under the License.
 */

package dogs

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"github.com/bwmarrin/discordgo"
	log "github.com/sirupsen/logrus"
	"gitlab.com/aguemort/ATSIR/structs"
	"gitlab.com/aguemort/ATSIR/utilities"
)

type dogSettingsStruct struct {
	APIKey string
}

type dogData []struct {
	ID               string  `json:"id"`
	URL              string  `json:"url"`
	Height           *int    `json:"height,omitempty"`
	Width            *int    `json:"width,omitempty"`
	SubID            *string `json:"sub_id,omitempty"`
	CreatedAt        *string `json:"created_at,omitempty"`
	OriginalFilename *string `json:"original_filename,omitempty"`
	Categories       *[]struct {
		ID   *int8  `json:"id,omitempty"`
		Name string `json:"name,omitempty"`
	} `json:"categories,omitempty"`
	Breeds []*struct {
		ID          *int    `json:"id,omitempty"`
		Name        *string `json:"name,omitempty"`
		AltNames    *string `json:"alt_names,omitempty"`
		BredFor     *string `json:"bred_for,omitempty"`
		BreedGroup  *string `json:"breed_group,omitempty"`
		CountryCode *string `json:"country_code,omitempty"`
		Description *string `json:"description,omitempty"`
		Height      *struct {
			Imperial *string `json:"imperial,omitempty"`
			Metric   *string `json:"metric,omitempty"`
		} `json:"height,omitempty"`
		History     *string `json:"history,omitempty"`
		LifeSpan    *string `json:"life_span,omitempty"`
		Origin      *string `json:"origin,omitempty"`
		Temperament *string `json:"temperament,omitempty"`
		Weight      *struct {
			Imperial *string `json:"imperial"`
			Metric   *string `json:"metric,omitempty"`
		} `json:"weight,omitempty"`
		WikipediaURL *string `json:"wikipedia_url,omitempty"`
	} `json:"breeds,omitempty"`
}

var dogSettings = &dogSettingsStruct{}

func init() {
	if _, err := os.Stat("data/dog.settings"); os.IsNotExist(err) {
		utilities.Info("Dogs settings file not found.  Creating it...")
		dogSettings.APIKey = "41b80795-3e7e-43e9-a6a0-0665db7b08e1"

		file, _ := json.MarshalIndent(dogSettings, "", "  ")
		_ = ioutil.WriteFile("data/dog.settings", file, 0644)
	}

	settingsJSON, _ := utilities.ReadJSONFile("data/dog.settings")
	_ = json.Unmarshal(settingsJSON, dogSettings)
}

// GetRandomDog - The primary handler for the dog command
func GetRandomDog(ch *structs.Command, args []string) error {
	client := http.DefaultClient
	request, err := http.NewRequest("GET", "https://api.thedogapi.com/v1/images/search", nil)
	if request == nil || err != nil {
		utilities.Warning("Error setting up the http request", err)
		return nil
	}
	request.Header.Set("x-api-key", dogSettings.APIKey)

	response, err := client.Do(request)
	if err != nil {
		utilities.Warning("Error processing the client request", err)
		return nil
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {

		}
	}(response.Body)

	if response == nil {
		utilities.Warning("We received a nil response", response)
		return nil
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Warningln("Error reading the response body", err)
	}
	imageData := dogData{}
	err = json.Unmarshal(body, &imageData)
	if err != nil {
		utilities.Warning("Error unmarshalling the response JSON", err)
		return nil
	}

	if len(imageData) > 0 {
		buildEmbed(ch, args, imageData)
	}

	return nil
}

func buildEmbed(ch *structs.Command, args []string, dog dogData) {
	var fields []*discordgo.MessageEmbedField
	c := dog[0]

	// The base embed
	embed := &discordgo.MessageEmbed{
		URL:         dog[0].URL,
		Type:        "rich",
		Title:       "",
		Description: "",
		Timestamp:   time.Now().Format(time.RFC3339),
		Color:       0,
		Footer: &discordgo.MessageEmbedFooter{
			Text:    "Dog image and data provided by: https://thedogapi.com/",
			IconURL: "https://cdn2.thedogapi.com/logos/wave-square_256.png",
		},
		Image: &discordgo.MessageEmbedImage{
			URL: c.URL,
		},
		Thumbnail: &discordgo.MessageEmbedThumbnail{
			URL: "https://cdn2.thedogapi.com/logos/wave-square_256.png",
		},
	}

	// Build the embed fields if additional metadata is provided from the API
	if len(c.Breeds) > 0 {
		b := c.Breeds[0]

		if b.BredFor != nil && *b.BredFor != "" {
			fields = append(fields, &discordgo.MessageEmbedField{
				Name:   "Bred For",
				Value:  *b.BredFor,
				Inline: true,
			})
		}
		if b.BreedGroup != nil && *b.BreedGroup != "" {
			fields = append(fields, &discordgo.MessageEmbedField{
				Name:   "Breed Group",
				Value:  *b.BreedGroup,
				Inline: true,
			})
		}
		if b.Description != nil && *b.Description != "" {
			embed.Description = *b.Description
		}
		if b.History != nil && *b.History != "" {
			if embed.Description == "" {
				embed.Description = *b.History
			} else {
				embed.Description += "\n\n" + *b.History
			}
		}
		if b.LifeSpan != nil && *b.LifeSpan != "" {
			fields = append(fields, &discordgo.MessageEmbedField{
				Name:   "Life Span",
				Value:  *b.LifeSpan,
				Inline: true,
			})
		}
		if b.Origin != nil && *b.Origin != "" {
			fields = append(fields, &discordgo.MessageEmbedField{
				Name:   "Origin",
				Value:  *b.Origin,
				Inline: true,
			})
		}
		if b.Temperament != nil && *b.Temperament != "" {
			fields = append(fields, &discordgo.MessageEmbedField{
				Name:   "Temperament",
				Value:  *b.Temperament,
				Inline: true,
			})
		}
	}

	// If no meta is returned for the image, we don't send the fields (for right now)
	if fields != nil {
		embed.Fields = fields
	}

	utilities.Tracef("%+v", embed)

	go utilities.SendEmbedMessage(ch, "", embed)
}
