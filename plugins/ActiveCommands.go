/*
 * Copyright 2019-2021 Keith Russo Licensed under the
 * 	Educational Community License, Version 2.0 (the "License"); you may
 * 	not use this file except in compliance with the License. You may
 * 	obtain a copy of the License at
 *
 * 	http://www.osedu.org/licenses/ECL-2.0
 *
 * 	Unless required by applicable law or agreed to in writing,
 * 	software distributed under the License is distributed on an "AS IS"
 * 	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * 	or implied. See the License for the specific language governing
 * 	permissions and limitations under the License.
 */

package plugins

import (
	"flag"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/aguemort/ATSIR/plugins/admin"
	"gitlab.com/aguemort/ATSIR/plugins/ascii"
	cat "gitlab.com/aguemort/ATSIR/plugins/cats"
	"gitlab.com/aguemort/ATSIR/plugins/dogs"
	"gitlab.com/aguemort/ATSIR/plugins/help"
	"gitlab.com/aguemort/ATSIR/plugins/invite"
	"gitlab.com/aguemort/ATSIR/plugins/movies"
	"gitlab.com/aguemort/ATSIR/plugins/starboard"
	"gitlab.com/aguemort/ATSIR/plugins/twitch"
	"gitlab.com/aguemort/ATSIR/structs"
)

// ASCII - The command structure for the ASCII art command
var ASCII = structs.Command{
	UsageLine:        "art [options]",
	ShortDescription: "Prints ASCII art for the specified word",
	LongDescription:  "",
	List:             structs.CommandsList,
	Run:              ascii.Art,
	Flag:             flag.FlagSet{}, // Currently unused
	SubCommands: []*structs.Command{
		{
			UsageLine:        "setfont [font-name]",
			ShortDescription: "Sets the font to be used with the ASCII art command",
			LongDescription:  "",
			List:             structs.CommandsList,
			Run:              ascii.SetFont,
			Flag:             flag.FlagSet{},
			SubCommands:      nil,
			Parent:           nil,
			UsageTemplate:    "",
			HelpFields: []*discordgo.MessageEmbedField{
				{
					Name: "Available Fonts",
					Value: "3-d,\t3x5,\t5lineoblique,\n" +
						"acrobatic,\talligator,\talligator2,\n" +
						"alphabet,\tavatar,\tbanner,\n" +
						"banner3,\tbanner3-D,\tbanner4,\n" +
						"barbwire,\tbasic,\tbell,\n" +
						"big,\tbigchief,\tbinary,\n" +
						"block,\tbubble,\tbulbhead,\n" +
						"calgphy2,\tcaligraphy,\tcatwalk,\n" +
						"chunky,\tcoinstak,\tcolossal,\n" +
						"computer,\tcontessa,\tcontrast,\n" +
						"cosmic,\tcricket,\n" +
						"cursive,\tcyberlarge,\tcybermedium,\n" +
						"cybersmall,\tdiamond,\tdigital,\n" +
						"doh,\tdoom,\tdotmatrix,\n" +
						"drpepper,\teftichess,\teftifont,\n" +
						"eftipiti,\teftirobot,\teftitalic,\n" +
						"eftiwall,\teftiwater,\telite,\n" +
						"epic,\tfender,\tfourtops,\n" +
						"fuzzy,\tgoofy,\tgothic,\n" +
						"graffiti,\thollywood,\tinvita,\n" +
						"isometric1,\tisometric2,\tisometric3,\n" +
						"isometric4,\titalic,\tivrit,\n" +
						"jazmine,\tjerusalem,\tkatakana,\n" +
						"kban,\tlarry3d,\tlcd,\n" +
						"lean,\tletters,\tlinux,\n" +
						"lockergnome,\tmadrid,\tmarquee,\n" +
						"maxfour,\tmike,\tmini,\n",
					Inline: true,
				},
				{
					Name: "Available Fonts con't...",
					Value: "mirror,\tmnemonic,\tmorse,\n" +
						"moscow,\tnancyj,\tnancyj-fancy,\n" +
						"nancyj-underlined,\tnipples,\tntgreek,\n" +
						"o8,\togre,\tpawp,\n" +
						"peaks,\tpebbles,\tpepper,\n" +
						"poison,\tpuffy,\tpyramid,\n" +
						"rectangles,\trelief,\trelief2,\n" +
						"rev,\troman,\trot13,\n" +
						"rounded,\trowancap,\trozzo,\n" +
						"runic,\trunyc,\tsblood,\n" +
						"script,\tserifcap,\tshadow,\n" +
						"short,\tslant,\tslide,\n" +
						"slscript,\tsmall,\tsmisone1,\n" +
						"smkeyboard,\tsmscript,\tsmshadow,\n" +
						"smslant,\tsmtengwar,\tspeed,\n" +
						"stampatello,\tstandard,\tstarwars,\n" +
						"stellar,\tstop,\tstraight,\n" +
						"tanja,\ttengwar,\tterm,\n" +
						"thick,\tthin,\tthreepoint,\n" +
						"ticks,\tticksslant,\ttinker-toy,\n" +
						"tombstone,\ttrek,\ttsalagi,\n" +
						"twopoint,\tunivers,\tusaflag,\n" +
						"wavy,\tweird",
					Inline: true,
				},
			},
			RequiredPermissions: nil,
		},
	},
	Parent:        nil,
	UsageTemplate: "",
	HelpFields: []*discordgo.MessageEmbedField{
		{
			Name:   "Available Sub-Commands",
			Value:  "setfont [font-name]",
			Inline: true,
		},
	},
	RequiredPermissions: nil,
}

// Cats - The command structure for the Cat command
var Cats = structs.Command{
	UsageLine:        "cat [options]",
	ShortDescription: "Displays a random cat image with some basic information about it (if provided)",
	LongDescription:  "",
	List:             structs.CommandsList,
	Run:              cat.GetRandomCat,
	Flag:             flag.FlagSet{},
	SubCommands:      nil,
	Parent:           nil,
	UsageTemplate:    "",
	HelpFields: []*discordgo.MessageEmbedField{
		{
			Name:   "Available Sub-Commands",
			Value:  "No sub-commands available",
			Inline: true,
		},
	},
	RequiredPermissions: nil,
}

// Dogs - The command structure for the Dog command
var Dogs = structs.Command{
	UsageLine:        "dog [options]",
	ShortDescription: "Displays a random dog image with some info about it (if provided)",
	LongDescription:  "",
	List:             structs.CommandsList,
	Run:              dogs.GetRandomDog,
	Flag:             flag.FlagSet{},
	SubCommands:      nil,
	Parent:           nil,
	UsageTemplate:    "",
	HelpFields: []*discordgo.MessageEmbedField{
		{
			Name:   "Available Sub-Commands",
			Value:  "No sub-commands available",
			Inline: true,
		},
	},
	RequiredPermissions: nil,
}

// Invite - The command structure for the Invite command
var Invite = structs.Command{
	UsageLine:        "invite",
	ShortDescription: "Displays the bot's invite link",
	LongDescription:  "",
	List:             structs.CommandsList,
	Run:              invite.GetInviteURL,
	Flag:             flag.FlagSet{},
	SubCommands:      nil,
	Parent:           nil,
	UsageTemplate:    "",
	HelpFields: []*discordgo.MessageEmbedField{
		{
			Name:   "Available Sub-Commands",
			Value:  "No sub-commands available",
			Inline: true,
		},
	},
	RequiredPermissions: nil,
}

// Twitch - The command structure for the Twitch command
var Twitch = structs.Command{
	UsageLine:        "twitch [options]",
	ShortDescription: "Query the Twitch REST API for the specified info. The base command has no effect",
	LongDescription:  "",
	List:             structs.CommandsList,
	Run:              twitch.Twitch,
	Flag:             flag.FlagSet{},
	SubCommands: []*structs.Command{
		{
			UsageLine: "streamer [streamer-name]",
			ShortDescription: "Returns information about a particular streamer. If they are currently live, it will " +
				"return information about their stream.",
			LongDescription: "",
			List:            structs.CommandsList,
			Run:             twitch.FetchUserByLogin,
			Flag:            flag.FlagSet{},
			SubCommands:     nil,
			Parent:          nil,
			UsageTemplate:   "",
			HelpFields: []*discordgo.MessageEmbedField{
				{
					Name: "Command Options",
					Value: "[streamer-name]\n" +
						"> The display name of the streamer (don't include the brackets)",
					Inline: true,
				},
			},
			RequiredPermissions: nil,
			Message:             nil,
		},
	},
	Parent:        nil,
	UsageTemplate: "",
	HelpFields: []*discordgo.MessageEmbedField{
		{
			Name:   "Available Sub-Commands",
			Value:  "streamer [streamer-name]",
			Inline: true,
		},
	},
	RequiredPermissions: nil,
}

// Movie - The command structure for the movie command
var Movie = structs.Command{
	UsageLine:        "movie [options]",
	ShortDescription: "Searches for information related to the specified movie",
	LongDescription:  "",
	List:             structs.CommandsList,
	Run:              movies.GetMovieData,
	Flag:             flag.FlagSet{},
	SubCommands:      nil,
	Parent:           nil,
	UsageTemplate:    "",
	HelpFields: []*discordgo.MessageEmbedField{
		{
			Name:   "Available Sub-Commands",
			Value:  "No sub-commands available",
			Inline: true,
		},
	},
	RequiredPermissions: nil,
}

// Help - The command structure for the Help command
var Help = structs.Command{
	UsageLine:        "help",
	ShortDescription: "Displays the bot's command list",
	LongDescription:  "",
	List:             structs.HelpTopicsList,
	Run:              help.Help,
	Flag:             flag.FlagSet{},
	SubCommands:      nil,
	Parent:           nil,
	UsageTemplate:    "",
	HelpFields: []*discordgo.MessageEmbedField{
		{
			Name:   "Available Sub-Commands",
			Value:  "No sub-commands available",
			Inline: true,
		},
	},
	RequiredPermissions: nil,
}

// Starboard - The command structure for the Starboard command
var Starboard = structs.Command{
	UsageLine:        "starboard [#channel]",
	ShortDescription: "Set's the target channel where the starboard will reside",
	LongDescription:  "",
	List:             structs.CommandsList,
	Run:              starboard.UpdateBoardChannel,
	Flag:             flag.FlagSet{},
	SubCommands:      nil,
	Parent:           nil,
	UsageTemplate:    "",
	HelpFields: []*discordgo.MessageEmbedField{
		{
			Name:   "Available Sub-Commands",
			Value:  "No sub-commands available",
			Inline: true,
		},
	},
	RequiredPermissions: nil,
}

// Admin - The command structure for the Starboard command
var Admin = structs.Command{
	UsageLine:        "admin [sub-command]",
	ShortDescription: "Set's certain bot parameters",
	LongDescription:  "",
	List:             structs.Unlisted,
	Run:              nil,
	Flag:             flag.FlagSet{},
	SubCommands: []*structs.Command{
		{
			UsageLine:        "logLevel [option]",
			ShortDescription: "Set's the target channel where the starboard will reside",
			LongDescription:  "",
			List:             structs.Unlisted,
			Run:              admin.LogLevel,
			Flag:             flag.FlagSet{},
			SubCommands:      nil,
			Parent:           nil,
			UsageTemplate:    "",
			HelpFields: []*discordgo.MessageEmbedField{
				{
					Name:   "Available Sub-Commands",
					Value:  "No sub-commands available",
					Inline: true,
				},
			},
			RequiredPermissions: nil,
		},
	},
	Parent:        nil,
	UsageTemplate: "",
	HelpFields: []*discordgo.MessageEmbedField{
		{
			Name:   "Available Sub-Commands",
			Value:  "logLevel",
			Inline: true,
		},
	},
	RequiredPermissions: nil,
}
