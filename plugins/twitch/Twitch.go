/*
 * Copyright 2019-2021 Keith Russo Licensed under the
 * 	Educational Community License, Version 2.0 (the "License"); you may
 * 	not use this file except in compliance with the License. You may
 * 	obtain a copy of the License at
 *
 * 	http://www.osedu.org/licenses/ECL-2.0
 *
 * 	Unless required by applicable law or agreed to in writing,
 * 	software distributed under the License is distributed on an "AS IS"
 * 	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * 	or implied. See the License for the specific language governing
 * 	permissions and limitations under the License.
 */

package twitch

import (
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/aguemort/ATSIR/structs"
	"gitlab.com/aguemort/ATSIR/utilities"
)

type user struct {
	Data []struct {
		BroadcasterType string `json:"broadcaster_type"`
		Description     string `json:"description"`
		DisplayName     string `json:"display_name"`
		Email           string `json:"email,omitempty"`
		ID              string `json:"id"`
		Login           string `json:"login"`
		OfflineImageURL string `json:"offline_image_url"`
		ProfileImageURL string `json:"profile_image_url"`
		Type            string `json:"type"`
		ViewCount       int    `json:"view_count"`
	} `json:"data"`
}

type stream struct {
	Data []struct {
		GameID       string   `json:"game_id"`
		ID           string   `json:"id"`
		Language     string   `json:"language"`
		Pagination   string   `json:"pagination"`
		StartedAt    string   `json:"started_at"`
		TagIds       []string `json:"tag_ids"`
		ThumbnailURL string   `json:"thumbnail_url"`
		Title        string   `json:"title"`
		Type         string   `json:"type"`
		UserID       string   `json:"user_id"`
		UserName     string   `json:"user_name"`
		ViewerCount  int      `json:"viewer_count"`
	} `json:"data"`
}

type game struct {
	Data []struct {
		BoxArtURL string `json:"box_art_url"`
		ID        string `json:"id"`
		Name      string `json:"name"`
	} `json:"data"`
}

type settingsStuct struct {
	ClientID string `json:"client_id"`
	Color    struct {
		Hex string `json:"hex"`
		Dec int    `json:"dec"`
	} `json:"color"`
}

var (
	settings     = &settingsStuct{}
	clientID     string
	baseURL      = "https://api.twitch.tv/helix"
	userRoute    = baseURL + "/users"
	streamsRoute = baseURL + "/streams"
	gamesRoute   = baseURL + "/games"
)

func init() {
	if _, err := os.Stat("data/twitch.settings"); os.IsNotExist(err) {
		utilities.Info("Twitch settings file not found.  Creating it...")
		settings.ClientID = "gvqmi6meq2f24bm5c2w98ssg1zso9l"
		settings.Color.Dec = 6570404
		settings.Color.Hex = "6441A4"

		file, _ := json.MarshalIndent(settings, "", "  ")
		_ = ioutil.WriteFile("data/twitch.settings", file, 0644)
	}

	settingsRaw, _ := utilities.ReadJSONFile("data/twitch.settings")
	err := json.Unmarshal(settingsRaw, &settings)
	if err != nil {
		utilities.Warning("Error unmarshalling Twitch API key string")
		return
	}

	clientID = settings.ClientID
}

// Twitch - Base command for the Twitch plugin
func Twitch(ch *structs.Command, args []string) error {
	utilities.Debug(args)
	if len(args) < 1 {
		utilities.Warning("No sub-command present")
		return errors.New("no sub-command present")
	}

	switch args[0] {
	case "user":
	case "streamer":
		utilities.Trace("Found twitch > user sub-command")
		_ = FetchUserByLogin(ch, args[1:])
	}

	return nil
}

// FetchUserByLogin - Primary handler for the Twitch User sub-command
func FetchUserByLogin(ch *structs.Command, args []string) error {
	utilities.Debug("Inside FetchUserByLogin()")
	if len(args) < 1 {
		utilities.Warning("No username passed.")
		return errors.New("no username passed")
	}

	client := http.DefaultClient
	request, err := http.NewRequest("GET", userRoute, nil)
	if err != nil {
		utilities.Warning("Error setting up Twitch request", err)
		return err
	}
	request.Header.Set("Client-ID", clientID)
	request.Header.Set("Authorization", "Bearer 5wipscms5diqts29jztqnaseg57d5e")

	query := request.URL.Query()
	query.Add("login", args[0])
	request.URL.RawQuery = query.Encode()

	utilities.Debug(request.URL.String())

	response, err := client.Do(request)
	if err != nil {
		utilities.Warning("Error processing the Twitch client request", err)
		return err
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {

		}
	}(response.Body)

	if response == nil {
		utilities.Warning("received an empty response from Twitch", response)
		return errors.New("received an empty response from Twitch")
	}
	if response.StatusCode == 404 {
		utilities.Warning("User not found", response)
		return errors.New("user not found")
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		utilities.Warning("Error reading the response body", err)
		return err
	}

	userData := &user{}
	err = json.Unmarshal(body, &userData)
	if err != nil {
		utilities.Warning("Error unmarshalling the response JSON", err)
		return err
	}

	if len(userData.Data) == 1 {
		buildUserEmbed(ch, userData, settings)
	}

	return nil
}

func checkUserStream(user *user) *stream {
	client := http.DefaultClient
	request, err := http.NewRequest("GET", streamsRoute, nil)
	if err != nil {
		utilities.Warning("Error setting up Twitch request", err)
		return nil
	}
	request.Header.Set("Client-ID", clientID)
	request.Header.Set("Authorization", "Bearer 5wipscms5diqts29jztqnaseg57d5e")

	query := request.URL.Query()
	query.Add("user_id", user.Data[0].ID)
	request.URL.RawQuery = query.Encode()

	response, err := client.Do(request)
	if err != nil {
		utilities.Warning("Error processing the Twitch client request", err)
		return nil
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {

		}
	}(response.Body)

	if response == nil {
		utilities.Warning("received an empty response from Twitch", response)
		return nil
	}
	if response.StatusCode == 404 {
		utilities.Warning("User not found", response)
		return nil
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		utilities.Warning("Error reading the response body", err)
		return nil
	}

	streamData := &stream{}
	err = json.Unmarshal(body, &streamData)
	if err != nil {
		utilities.Warning("Error unmarshalling the response JSON", err)
		return nil
	}

	return streamData
}

func fetchGameData(gameID string) *game {
	client := http.DefaultClient
	request, err := http.NewRequest("GET", gamesRoute, nil)
	if err != nil {
		utilities.Warning("Error setting up Twitch request", err)
		return &game{}
	}
	request.Header.Set("Client-ID", clientID)
	request.Header.Set("Authorization", "Bearer 5wipscms5diqts29jztqnaseg57d5e")

	query := request.URL.Query()
	query.Add("id", gameID)
	request.URL.RawQuery = query.Encode()

	response, err := client.Do(request)
	if err != nil {
		utilities.Warning("Error receiving response from Twitch", err)
		return &game{}
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {

		}
	}(response.Body)

	if response == nil {
		utilities.Warning("Received an empty response body from Twitch", response)
		return &game{}
	}

	if response.StatusCode == 404 {
		utilities.Warning("Game not found", response)
		return &game{}
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		utilities.Warning("Error reading the response body", err)
		return &game{}
	}

	gameData := &game{}
	err = json.Unmarshal(body, &gameData)
	if err != nil {
		utilities.Warning("Error unmarshalling games response body", err)
		return &game{}
	}

	return gameData
}

func buildUserEmbed(ch *structs.Command, user *user, settings *settingsStuct) {
	u := user.Data[0]

	var image string
	var fields []*discordgo.MessageEmbedField

	stream := checkUserStream(user)

	utilities.Tracef("%+v", stream)
	utilities.Trace("Number of results returned:", len(stream.Data))

	if stream != nil && len(stream.Data) > 0 {
		s := stream.Data[0]
		image = strings.Replace(s.ThumbnailURL, "-{width}x{height}", "", 1)

		// Fetch the game name; if the game doesn't exist on Twitch then we just state an unknown game
		gameName := "Unknown game"
		if s.GameID != "" {
			game := fetchGameData(s.GameID)
			if len(game.Data) == 1 {
				g := game.Data[0]
				if g.ID != "" && g.Name != "" {
					gameName = g.Name
				}
			}
		}

		fields = append(fields, &discordgo.MessageEmbedField{
			Name:   " :video_game: Streaming :video_game: ",
			Value:  gameName,
			Inline: false,
		})
		// Time parsing
		t, _ := time.Parse(time.RFC3339, s.StartedAt)
		fields = append(fields, &discordgo.MessageEmbedField{
			Name:   "Started Streaming At",
			Value:  t.Format(time.RFC822),
			Inline: true,
		})
		fields = append(fields, &discordgo.MessageEmbedField{
			Name:   " :eyes: Current Viewers :eyes: ",
			Value:  strconv.Itoa(s.ViewerCount),
			Inline: true,
		})
	} else {
		image = u.OfflineImageURL
		fields = append(fields, &discordgo.MessageEmbedField{
			Name:   "Total Views",
			Value:  strconv.Itoa(u.ViewCount),
			Inline: true,
		})
	}

	embed := &discordgo.MessageEmbed{
		URL:         "https://twitch.tv/" + u.Login,
		Type:        "rich",
		Title:       u.DisplayName,
		Description: u.Description,
		Timestamp:   time.Now().Format(time.RFC3339),
		Color:       settings.Color.Dec,
		Footer: &discordgo.MessageEmbedFooter{
			Text:    "Command called by: " + ch.Message.Author.Username,
			IconURL: ch.Message.Author.AvatarURL(""),
		},
		Image: &discordgo.MessageEmbedImage{
			URL: image,
		},
		Thumbnail: &discordgo.MessageEmbedThumbnail{
			URL: u.ProfileImageURL,
		},
		Fields: fields,
	}

	go utilities.SendEmbedMessage(ch, "", embed)
}
