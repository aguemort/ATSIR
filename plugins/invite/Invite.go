/*
 * Copyright 2019-2021 Keith Russo Licensed under the
 * 	Educational Community License, Version 2.0 (the "License"); you may
 * 	not use this file except in compliance with the License. You may
 * 	obtain a copy of the License at
 *
 * 	http://www.osedu.org/licenses/ECL-2.0
 *
 * 	Unless required by applicable law or agreed to in writing,
 * 	software distributed under the License is distributed on an "AS IS"
 * 	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * 	or implied. See the License for the specific language governing
 * 	permissions and limitations under the License.
 */

package invite

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/aguemort/ATSIR/structs"
	"gitlab.com/aguemort/ATSIR/utilities"
)

// GetInviteURL - The primary handler for the Invite command
func GetInviteURL(ch *structs.Command, args []string) error {
	embed := &discordgo.MessageEmbed{
		URL:         "https://discordapp.com/oauth2/authorize?&client_id=" + utilities.Settings.DiscordClientID + "&scope=bot&permissions=8",
		Type:        "rich",
		Title:       utilities.Settings.BotName + " Invite Link",
		Description: "[Heya! Click here to invite me to your server!](https://discordapp.com/oauth2/authorize?&client_id=" + utilities.Settings.DiscordClientID + "&scope=bot&permissions=8)",
	}

	utilities.SendEmbedMessage(ch, "", embed)

	return nil
}
