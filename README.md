[![Pipeline Status](https://gitlab.com/aguemort/atsir/badges/master/pipeline.svg)](https://gitlab.com/aguemort/atsir/-/commits/master)
[![License ECS](https://img.shields.io/badge/License-ECLv2.0-brightgreen.svg)](https://opensource.org/licenses/ECL-2.0)
![GitLab tag (latest by SemVer)](https://img.shields.io/gitlab/v/tag/aguemort/ATSIR?label=Latest%20Version)

## ATSIR - Almighty Tallest Supreme Invasion Robot

> This is my final project for CSC-160 at Baldwin Wallace University. Pull requests are not accepted. May be forked for educational purposes only

## Credits

### Code

All code written by Keith Russo &copy;2019-2021

### Logo

Design and implementation by MikeyTopCat <https://twitch.tv/mikeytopcat> &copy;2019