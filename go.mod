module gitlab.com/aguemort/ATSIR

go 1.17

require (
	github.com/VividCortex/ewma v1.2.0 // indirect
	github.com/bwmarrin/discordgo v0.23.2
	github.com/cheggaaa/pb/v3 v3.0.8
	github.com/common-nighthawk/go-figure v0.0.0-20210622060536-734e95fb86be
	github.com/fatih/color v1.13.0 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/gosimple/slug v1.10.0
	github.com/mattn/go-colorable v0.1.9
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/sys v0.0.0-20210923061019-b8560ed6a9b7 // indirect
)

require (
	github.com/gosimple/unidecode v1.0.0 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
)
